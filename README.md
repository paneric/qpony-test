# qpony - test

### composer
```sh
$ git clone https://paneric@bitbucket.org/paneric/qpony-test.git
$ cd qpony-test
$ composer install
$ php -S localhost:8000 -t public
```
### browser
```sh
http://localhost:8000/max_number/find
```