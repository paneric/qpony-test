<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

class MaxNumberService
{
    public function find(Request $request): ?array
    {
        if ($request->getMethod() === 'POST') {

            $sequence = array_filter(
                $request->get('sequence'),
                static function($nbr) {
                    return ctype_digit($nbr) && (0 < (int) $nbr && (int) $nbr < 100000);
                }
            );

            return [
                'sequence' => implode(',', $sequence),
                'max_number' => max($sequence),
            ];
        }

        return null;
    }
}
