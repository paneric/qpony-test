<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\MaxNumberService;

use Symfony\Component\HttpFoundation\Response;
use App\Website\Service\HomeService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class MaxNumberController extends AbstractController
{
    private $maxNumberService;

    public function __construct(MaxNumberService $maxNumberService)
    {
        $this->maxNumberService = $maxNumberService;

    }

    public function find(Request $request): Response
    {
        $result = $this->maxNumberService->find($request);

        if ($result === null) {
            return $this->render(
                'find.html.twig'
            );
        }

        return $this->render(
            'index.html.twig',
            $result
        );
    }
}